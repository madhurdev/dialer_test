// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: rtt_transcript.proto

package com.android.dialer.rtt;

public final class RttTranscriptOuterClass {
  private RttTranscriptOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_android_dialer_rtt_RttTranscript_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_android_dialer_rtt_RttTranscript_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_android_dialer_rtt_RttTranscriptMessage_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_android_dialer_rtt_RttTranscriptMessage_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\024rtt_transcript.proto\022\026com.android.dial" +
      "er.rtt\"~\n\rRttTranscript\022\n\n\002id\030\001 \001(\t\022\016\n\006n" +
      "umber\030\002 \001(\t\022\021\n\ttimestamp\030\003 \001(\003\022>\n\010messag" +
      "es\030\004 \003(\0132,.com.android.dialer.rtt.RttTra" +
      "nscriptMessage\"b\n\024RttTranscriptMessage\022\017" +
      "\n\007content\030\001 \001(\t\022\021\n\ttimestamp\030\002 \001(\003\022\021\n\tis" +
      "_remote\030\003 \001(\010\022\023\n\013is_finished\030\004 \001(\010B\032\n\026co" +
      "m.android.dialer.rttP\001"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_android_dialer_rtt_RttTranscript_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_android_dialer_rtt_RttTranscript_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_android_dialer_rtt_RttTranscript_descriptor,
        new java.lang.String[] { "Id", "Number", "Timestamp", "Messages", });
    internal_static_com_android_dialer_rtt_RttTranscriptMessage_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_com_android_dialer_rtt_RttTranscriptMessage_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_android_dialer_rtt_RttTranscriptMessage_descriptor,
        new java.lang.String[] { "Content", "Timestamp", "IsRemote", "IsFinished", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
