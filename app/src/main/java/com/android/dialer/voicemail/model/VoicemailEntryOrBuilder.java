// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: voicemail_entry.proto

package com.android.dialer.voicemail.model;

public interface VoicemailEntryOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.android.dialer.voicemail.model.VoicemailEntry)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * Value in column AnnotatedCallLog._ID
   * </pre>
   *
   * <code>optional int64 id = 1;</code>
   * @return Whether the id field is set.
   */
  boolean hasId();
  /**
   * <pre>
   * Value in column AnnotatedCallLog._ID
   * </pre>
   *
   * <code>optional int64 id = 1;</code>
   * @return The id.
   */
  long getId();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.TIMESTAMP
   * </pre>
   *
   * <code>optional int64 timestamp = 2;</code>
   * @return Whether the timestamp field is set.
   */
  boolean hasTimestamp();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.TIMESTAMP
   * </pre>
   *
   * <code>optional int64 timestamp = 2;</code>
   * @return The timestamp.
   */
  long getTimestamp();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.NUMBER
   * </pre>
   *
   * <code>optional .com.android.dialer.DialerPhoneNumber number = 3;</code>
   * @return Whether the number field is set.
   */
  boolean hasNumber();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.NUMBER
   * </pre>
   *
   * <code>optional .com.android.dialer.DialerPhoneNumber number = 3;</code>
   * @return The number.
   */
  com.android.dialer.DialerPhoneNumber getNumber();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.NUMBER
   * </pre>
   *
   * <code>optional .com.android.dialer.DialerPhoneNumber number = 3;</code>
   */
  com.android.dialer.DialerPhoneNumberOrBuilder getNumberOrBuilder();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.FORMATTED_NUMBER
   * </pre>
   *
   * <code>optional string formatted_number = 4;</code>
   * @return Whether the formattedNumber field is set.
   */
  boolean hasFormattedNumber();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.FORMATTED_NUMBER
   * </pre>
   *
   * <code>optional string formatted_number = 4;</code>
   * @return The formattedNumber.
   */
  java.lang.String getFormattedNumber();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.FORMATTED_NUMBER
   * </pre>
   *
   * <code>optional string formatted_number = 4;</code>
   * @return The bytes for formattedNumber.
   */
  com.google.protobuf.ByteString
      getFormattedNumberBytes();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.GEOCODED_LOCATION
   * </pre>
   *
   * <code>optional string geocoded_location = 5;</code>
   * @return Whether the geocodedLocation field is set.
   */
  boolean hasGeocodedLocation();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.GEOCODED_LOCATION
   * </pre>
   *
   * <code>optional string geocoded_location = 5;</code>
   * @return The geocodedLocation.
   */
  java.lang.String getGeocodedLocation();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.GEOCODED_LOCATION
   * </pre>
   *
   * <code>optional string geocoded_location = 5;</code>
   * @return The bytes for geocodedLocation.
   */
  com.google.protobuf.ByteString
      getGeocodedLocationBytes();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.DURATION
   * </pre>
   *
   * <code>optional int64 duration = 6;</code>
   * @return Whether the duration field is set.
   */
  boolean hasDuration();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.DURATION
   * </pre>
   *
   * <code>optional int64 duration = 6;</code>
   * @return The duration.
   */
  long getDuration();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.TRANSCRIPTION
   * </pre>
   *
   * <code>optional string transcription = 7;</code>
   * @return Whether the transcription field is set.
   */
  boolean hasTranscription();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.TRANSCRIPTION
   * </pre>
   *
   * <code>optional string transcription = 7;</code>
   * @return The transcription.
   */
  java.lang.String getTranscription();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.TRANSCRIPTION
   * </pre>
   *
   * <code>optional string transcription = 7;</code>
   * @return The bytes for transcription.
   */
  com.google.protobuf.ByteString
      getTranscriptionBytes();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.VOICEMAIL_URI
   * </pre>
   *
   * <code>optional string voicemail_uri = 8;</code>
   * @return Whether the voicemailUri field is set.
   */
  boolean hasVoicemailUri();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.VOICEMAIL_URI
   * </pre>
   *
   * <code>optional string voicemail_uri = 8;</code>
   * @return The voicemailUri.
   */
  java.lang.String getVoicemailUri();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.VOICEMAIL_URI
   * </pre>
   *
   * <code>optional string voicemail_uri = 8;</code>
   * @return The bytes for voicemailUri.
   */
  com.google.protobuf.ByteString
      getVoicemailUriBytes();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.CALL_TYPE
   * </pre>
   *
   * <code>optional int32 call_type = 9;</code>
   * @return Whether the callType field is set.
   */
  boolean hasCallType();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.CALL_TYPE
   * </pre>
   *
   * <code>optional int32 call_type = 9;</code>
   * @return The callType.
   */
  int getCallType();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.IS_READ
   * </pre>
   *
   * <code>optional int32 is_read = 10;</code>
   * @return Whether the isRead field is set.
   */
  boolean hasIsRead();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.IS_READ
   * </pre>
   *
   * <code>optional int32 is_read = 10;</code>
   * @return The isRead.
   */
  int getIsRead();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.NUMBER_ATTRIBUTES
   * </pre>
   *
   * <code>optional .com.android.dialer.NumberAttributes number_attributes = 11;</code>
   * @return Whether the numberAttributes field is set.
   */
  boolean hasNumberAttributes();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.NUMBER_ATTRIBUTES
   * </pre>
   *
   * <code>optional .com.android.dialer.NumberAttributes number_attributes = 11;</code>
   * @return The numberAttributes.
   */
  com.android.dialer.NumberAttributes getNumberAttributes();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.NUMBER_ATTRIBUTES
   * </pre>
   *
   * <code>optional .com.android.dialer.NumberAttributes number_attributes = 11;</code>
   */
  com.android.dialer.NumberAttributesOrBuilder getNumberAttributesOrBuilder();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.TRANSCRIPTION_STATE
   * </pre>
   *
   * <code>optional int32 transcription_state = 12;</code>
   * @return Whether the transcriptionState field is set.
   */
  boolean hasTranscriptionState();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.TRANSCRIPTION_STATE
   * </pre>
   *
   * <code>optional int32 transcription_state = 12;</code>
   * @return The transcriptionState.
   */
  int getTranscriptionState();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.PHONE_ACCOUNT_COMPONENT_NAME
   * </pre>
   *
   * <code>optional string phone_account_component_name = 13;</code>
   * @return Whether the phoneAccountComponentName field is set.
   */
  boolean hasPhoneAccountComponentName();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.PHONE_ACCOUNT_COMPONENT_NAME
   * </pre>
   *
   * <code>optional string phone_account_component_name = 13;</code>
   * @return The phoneAccountComponentName.
   */
  java.lang.String getPhoneAccountComponentName();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.PHONE_ACCOUNT_COMPONENT_NAME
   * </pre>
   *
   * <code>optional string phone_account_component_name = 13;</code>
   * @return The bytes for phoneAccountComponentName.
   */
  com.google.protobuf.ByteString
      getPhoneAccountComponentNameBytes();

  /**
   * <pre>
   * Value in column AnnotatedCallLog.PHONE_ACCOUNT_ID
   * </pre>
   *
   * <code>optional string phone_account_id = 14;</code>
   * @return Whether the phoneAccountId field is set.
   */
  boolean hasPhoneAccountId();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.PHONE_ACCOUNT_ID
   * </pre>
   *
   * <code>optional string phone_account_id = 14;</code>
   * @return The phoneAccountId.
   */
  java.lang.String getPhoneAccountId();
  /**
   * <pre>
   * Value in column AnnotatedCallLog.PHONE_ACCOUNT_ID
   * </pre>
   *
   * <code>optional string phone_account_id = 14;</code>
   * @return The bytes for phoneAccountId.
   */
  com.google.protobuf.ByteString
      getPhoneAccountIdBytes();
}
