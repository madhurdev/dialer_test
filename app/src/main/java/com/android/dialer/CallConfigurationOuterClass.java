// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: call_configuration.proto

package com.android.dialer;

public final class CallConfigurationOuterClass {
  private CallConfigurationOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_android_dialer_CallConfiguration_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_android_dialer_CallConfiguration_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\030call_configuration.proto\022\022com.android." +
      "dialer\"@\n\021CallConfiguration\022+\n\tcall_mode" +
      "\030\001 \001(\0162\030.com.android.dialer.Mode*(\n\004Mode" +
      "\022\024\n\020MODE_UNSPECIFIED\020\000\022\n\n\006BUBBLE\020\001B\026\n\022co" +
      "m.android.dialerP\001"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_android_dialer_CallConfiguration_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_android_dialer_CallConfiguration_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_android_dialer_CallConfiguration_descriptor,
        new java.lang.String[] { "CallMode", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
