// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: speed_dial_contact_type.proto

package com.android.dialer.callintent;

public final class SpeedDialContactTypeOuterClass {
  private SpeedDialContactTypeOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_android_dialer_callintent_SpeedDialContactType_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_android_dialer_callintent_SpeedDialContactType_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\035speed_dial_contact_type.proto\022\035com.and" +
      "roid.dialer.callintent\"l\n\024SpeedDialConta" +
      "ctType\"T\n\004Type\022\r\n\tUNDEFINED\020\000\022\022\n\016PINNED_" +
      "CONTACT\020\001\022\023\n\017STARRED_CONTACT\020\002\022\024\n\020FREQUE" +
      "NT_CONTACT\020\003B!\n\035com.android.dialer.calli" +
      "ntentP\001"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_android_dialer_callintent_SpeedDialContactType_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_android_dialer_callintent_SpeedDialContactType_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_android_dialer_callintent_SpeedDialContactType_descriptor,
        new java.lang.String[] { });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
