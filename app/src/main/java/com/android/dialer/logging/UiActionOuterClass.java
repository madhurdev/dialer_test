// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ui_action.proto

package com.android.dialer.logging;

public final class UiActionOuterClass {
  private UiActionOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_android_dialer_logging_UiAction_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_android_dialer_logging_UiAction_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\017ui_action.proto\022\032com.android.dialer.lo" +
      "gging\"\344\004\n\010UiAction\"\327\004\n\004Type\022\013\n\007UNKNOWN\020\000" +
      "\022\032\n\026CHANGE_TAB_TO_FAVORITE\020\001\022\032\n\026CHANGE_T" +
      "AB_TO_CALL_LOG\020\002\022\032\n\026CHANGE_TAB_TO_CONTAC" +
      "TS\020\003\022\033\n\027CHANGE_TAB_TO_VOICEMAIL\020\004\022\035\n\031PRE" +
      "SS_ANDROID_BACK_BUTTON\020\005\022\032\n\026TEXT_CHANGE_" +
      "WITH_INPUT\020\006\022\n\n\006SCROLL\020\007\022\027\n\023CLICK_CALL_L" +
      "OG_ITEM\020d\022\024\n\020OPEN_CALL_DETAIL\020e\022(\n$CLOSE" +
      "_CALL_DETAIL_WITH_CANCEL_BUTTON\020f\022\036\n\032COP" +
      "Y_NUMBER_IN_CALL_DETAIL\020g\022*\n&EDIT_NUMBER" +
      "_BEFORE_CALL_IN_CALL_DETAIL\020h\022\021\n\014OPEN_DI" +
      "ALPAD\020\310\001\022\022\n\rCLOSE_DIALPAD\020\311\001\022&\n!PRESS_CA" +
      "LL_BUTTON_WITHOUT_CALLING\020\312\001\022\020\n\013OPEN_SEA" +
      "RCH\020\254\002\022\034\n\027HIDE_KEYBOARD_IN_SEARCH\020\255\002\022\"\n\035" +
      "CLOSE_SEARCH_WITH_HIDE_BUTTON\020\256\002\022\026\n\021OPEN" +
      "_CALL_HISTORY\020\220\003\022*\n%CLOSE_CALL_HISTORY_W" +
      "ITH_CANCEL_BUTTON\020\221\003B\036\n\032com.android.dial" +
      "er.loggingP\001"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_android_dialer_logging_UiAction_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_android_dialer_logging_UiAction_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_android_dialer_logging_UiAction_descriptor,
        new java.lang.String[] { });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
