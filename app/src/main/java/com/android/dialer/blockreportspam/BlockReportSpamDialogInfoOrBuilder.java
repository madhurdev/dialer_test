// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: block_report_spam_dialog_info.proto

package com.android.dialer.blockreportspam;

public interface BlockReportSpamDialogInfoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.android.dialer.blockreportspam.BlockReportSpamDialogInfo)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * A dialer-normalized version of the number used in the dialogs.
   * See DialerPhoneNumber#normalized_number.
   * </pre>
   *
   * <code>optional string normalized_number = 1;</code>
   * @return Whether the normalizedNumber field is set.
   */
  boolean hasNormalizedNumber();
  /**
   * <pre>
   * A dialer-normalized version of the number used in the dialogs.
   * See DialerPhoneNumber#normalized_number.
   * </pre>
   *
   * <code>optional string normalized_number = 1;</code>
   * @return The normalizedNumber.
   */
  java.lang.String getNormalizedNumber();
  /**
   * <pre>
   * A dialer-normalized version of the number used in the dialogs.
   * See DialerPhoneNumber#normalized_number.
   * </pre>
   *
   * <code>optional string normalized_number = 1;</code>
   * @return The bytes for normalizedNumber.
   */
  com.google.protobuf.ByteString
      getNormalizedNumberBytes();

  /**
   * <pre>
   * The ISO 3166-1 two letters country code of the number.
   * </pre>
   *
   * <code>optional string country_iso = 2;</code>
   * @return Whether the countryIso field is set.
   */
  boolean hasCountryIso();
  /**
   * <pre>
   * The ISO 3166-1 two letters country code of the number.
   * </pre>
   *
   * <code>optional string country_iso = 2;</code>
   * @return The countryIso.
   */
  java.lang.String getCountryIso();
  /**
   * <pre>
   * The ISO 3166-1 two letters country code of the number.
   * </pre>
   *
   * <code>optional string country_iso = 2;</code>
   * @return The bytes for countryIso.
   */
  com.google.protobuf.ByteString
      getCountryIsoBytes();

  /**
   * <pre>
   * Type of the call to/from the number, as defined in
   * android.provider.CallLog.Calls
   * </pre>
   *
   * <code>optional int32 call_type = 3;</code>
   * @return Whether the callType field is set.
   */
  boolean hasCallType();
  /**
   * <pre>
   * Type of the call to/from the number, as defined in
   * android.provider.CallLog.Calls
   * </pre>
   *
   * <code>optional int32 call_type = 3;</code>
   * @return The callType.
   */
  int getCallType();

  /**
   * <pre>
   * The location where the number is reported.
   * </pre>
   *
   * <code>optional .com.android.dialer.logging.ReportingLocation.Type reporting_location = 4;</code>
   * @return Whether the reportingLocation field is set.
   */
  boolean hasReportingLocation();
  /**
   * <pre>
   * The location where the number is reported.
   * </pre>
   *
   * <code>optional .com.android.dialer.logging.ReportingLocation.Type reporting_location = 4;</code>
   * @return The reportingLocation.
   */
  com.android.dialer.logging.ReportingLocation.Type getReportingLocation();

  /**
   * <pre>
   * The source where contact info is associated with the number.
   * </pre>
   *
   * <code>optional .com.android.dialer.logging.ContactSource.Type contact_source = 5;</code>
   * @return Whether the contactSource field is set.
   */
  boolean hasContactSource();
  /**
   * <pre>
   * The source where contact info is associated with the number.
   * </pre>
   *
   * <code>optional .com.android.dialer.logging.ContactSource.Type contact_source = 5;</code>
   * @return The contactSource.
   */
  com.android.dialer.logging.ContactSource.Type getContactSource();
}
