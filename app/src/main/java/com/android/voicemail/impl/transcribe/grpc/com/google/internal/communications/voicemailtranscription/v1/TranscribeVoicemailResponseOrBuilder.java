// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: voicemail_transcription.proto

package com.google.internal.communications.voicemailtranscription.v1;

public interface TranscribeVoicemailResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:google.internal.communications.voicemailtranscription.v1.TranscribeVoicemailResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * The transcribed text of the voicemail.
   * </pre>
   *
   * <code>optional string transcript = 1;</code>
   * @return Whether the transcript field is set.
   */
  boolean hasTranscript();
  /**
   * <pre>
   * The transcribed text of the voicemail.
   * </pre>
   *
   * <code>optional string transcript = 1;</code>
   * @return The transcript.
   */
  java.lang.String getTranscript();
  /**
   * <pre>
   * The transcribed text of the voicemail.
   * </pre>
   *
   * <code>optional string transcript = 1;</code>
   * @return The bytes for transcript.
   */
  com.google.protobuf.ByteString
      getTranscriptBytes();
}
