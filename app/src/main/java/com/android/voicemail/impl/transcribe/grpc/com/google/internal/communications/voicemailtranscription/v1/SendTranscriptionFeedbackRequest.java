// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: voicemail_transcription.proto

package com.google.internal.communications.voicemailtranscription.v1;

/**
 * <pre>
 * Request for uploading transcription ratings.
 * </pre>
 *
 * Protobuf type {@code google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest}
 */
public final class SendTranscriptionFeedbackRequest extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest)
    SendTranscriptionFeedbackRequestOrBuilder {
private static final long serialVersionUID = 0L;
  // Use SendTranscriptionFeedbackRequest.newBuilder() to construct.
  private SendTranscriptionFeedbackRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private SendTranscriptionFeedbackRequest() {
    rating_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new SendTranscriptionFeedbackRequest();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private SendTranscriptionFeedbackRequest(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            if (!((mutable_bitField0_ & 0x00000001) != 0)) {
              rating_ = new java.util.ArrayList<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating>();
              mutable_bitField0_ |= 0x00000001;
            }
            rating_.add(
                input.readMessage(com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.PARSER, extensionRegistry));
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) != 0)) {
        rating_ = java.util.Collections.unmodifiableList(rating_);
      }
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.google.internal.communications.voicemailtranscription.v1.VoicemailTranscription.internal_static_google_internal_communications_voicemailtranscription_v1_SendTranscriptionFeedbackRequest_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.google.internal.communications.voicemailtranscription.v1.VoicemailTranscription.internal_static_google_internal_communications_voicemailtranscription_v1_SendTranscriptionFeedbackRequest_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.class, com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.Builder.class);
  }

  public static final int RATING_FIELD_NUMBER = 1;
  private java.util.List<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating> rating_;
  /**
   * <pre>
   * User feedback indicating the transcription quality for one or more
   * voicemails
   * </pre>
   *
   * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
   */
  @java.lang.Override
  public java.util.List<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating> getRatingList() {
    return rating_;
  }
  /**
   * <pre>
   * User feedback indicating the transcription quality for one or more
   * voicemails
   * </pre>
   *
   * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
   */
  @java.lang.Override
  public java.util.List<? extends com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder> 
      getRatingOrBuilderList() {
    return rating_;
  }
  /**
   * <pre>
   * User feedback indicating the transcription quality for one or more
   * voicemails
   * </pre>
   *
   * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
   */
  @java.lang.Override
  public int getRatingCount() {
    return rating_.size();
  }
  /**
   * <pre>
   * User feedback indicating the transcription quality for one or more
   * voicemails
   * </pre>
   *
   * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
   */
  @java.lang.Override
  public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating getRating(int index) {
    return rating_.get(index);
  }
  /**
   * <pre>
   * User feedback indicating the transcription quality for one or more
   * voicemails
   * </pre>
   *
   * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
   */
  @java.lang.Override
  public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder getRatingOrBuilder(
      int index) {
    return rating_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < rating_.size(); i++) {
      output.writeMessage(1, rating_.get(i));
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < rating_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, rating_.get(i));
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest)) {
      return super.equals(obj);
    }
    com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest other = (com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest) obj;

    if (!getRatingList()
        .equals(other.getRatingList())) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (getRatingCount() > 0) {
      hash = (37 * hash) + RATING_FIELD_NUMBER;
      hash = (53 * hash) + getRatingList().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Request for uploading transcription ratings.
   * </pre>
   *
   * Protobuf type {@code google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest)
      com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequestOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.google.internal.communications.voicemailtranscription.v1.VoicemailTranscription.internal_static_google_internal_communications_voicemailtranscription_v1_SendTranscriptionFeedbackRequest_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.google.internal.communications.voicemailtranscription.v1.VoicemailTranscription.internal_static_google_internal_communications_voicemailtranscription_v1_SendTranscriptionFeedbackRequest_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.class, com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.Builder.class);
    }

    // Construct using com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
        getRatingFieldBuilder();
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      if (ratingBuilder_ == null) {
        rating_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
      } else {
        ratingBuilder_.clear();
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.google.internal.communications.voicemailtranscription.v1.VoicemailTranscription.internal_static_google_internal_communications_voicemailtranscription_v1_SendTranscriptionFeedbackRequest_descriptor;
    }

    @java.lang.Override
    public com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest getDefaultInstanceForType() {
      return com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.getDefaultInstance();
    }

    @java.lang.Override
    public com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest build() {
      com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest buildPartial() {
      com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest result = new com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest(this);
      int from_bitField0_ = bitField0_;
      if (ratingBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0)) {
          rating_ = java.util.Collections.unmodifiableList(rating_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.rating_ = rating_;
      } else {
        result.rating_ = ratingBuilder_.build();
      }
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest) {
        return mergeFrom((com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest other) {
      if (other == com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest.getDefaultInstance()) return this;
      if (ratingBuilder_ == null) {
        if (!other.rating_.isEmpty()) {
          if (rating_.isEmpty()) {
            rating_ = other.rating_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureRatingIsMutable();
            rating_.addAll(other.rating_);
          }
          onChanged();
        }
      } else {
        if (!other.rating_.isEmpty()) {
          if (ratingBuilder_.isEmpty()) {
            ratingBuilder_.dispose();
            ratingBuilder_ = null;
            rating_ = other.rating_;
            bitField0_ = (bitField0_ & ~0x00000001);
            ratingBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getRatingFieldBuilder() : null;
          } else {
            ratingBuilder_.addAllMessages(other.rating_);
          }
        }
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.util.List<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating> rating_ =
      java.util.Collections.emptyList();
    private void ensureRatingIsMutable() {
      if (!((bitField0_ & 0x00000001) != 0)) {
        rating_ = new java.util.ArrayList<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating>(rating_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder> ratingBuilder_;

    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public java.util.List<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating> getRatingList() {
      if (ratingBuilder_ == null) {
        return java.util.Collections.unmodifiableList(rating_);
      } else {
        return ratingBuilder_.getMessageList();
      }
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public int getRatingCount() {
      if (ratingBuilder_ == null) {
        return rating_.size();
      } else {
        return ratingBuilder_.getCount();
      }
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating getRating(int index) {
      if (ratingBuilder_ == null) {
        return rating_.get(index);
      } else {
        return ratingBuilder_.getMessage(index);
      }
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder setRating(
        int index, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating value) {
      if (ratingBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureRatingIsMutable();
        rating_.set(index, value);
        onChanged();
      } else {
        ratingBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder setRating(
        int index, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder builderForValue) {
      if (ratingBuilder_ == null) {
        ensureRatingIsMutable();
        rating_.set(index, builderForValue.build());
        onChanged();
      } else {
        ratingBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder addRating(com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating value) {
      if (ratingBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureRatingIsMutable();
        rating_.add(value);
        onChanged();
      } else {
        ratingBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder addRating(
        int index, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating value) {
      if (ratingBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureRatingIsMutable();
        rating_.add(index, value);
        onChanged();
      } else {
        ratingBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder addRating(
        com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder builderForValue) {
      if (ratingBuilder_ == null) {
        ensureRatingIsMutable();
        rating_.add(builderForValue.build());
        onChanged();
      } else {
        ratingBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder addRating(
        int index, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder builderForValue) {
      if (ratingBuilder_ == null) {
        ensureRatingIsMutable();
        rating_.add(index, builderForValue.build());
        onChanged();
      } else {
        ratingBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder addAllRating(
        java.lang.Iterable<? extends com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating> values) {
      if (ratingBuilder_ == null) {
        ensureRatingIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, rating_);
        onChanged();
      } else {
        ratingBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder clearRating() {
      if (ratingBuilder_ == null) {
        rating_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        ratingBuilder_.clear();
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public Builder removeRating(int index) {
      if (ratingBuilder_ == null) {
        ensureRatingIsMutable();
        rating_.remove(index);
        onChanged();
      } else {
        ratingBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder getRatingBuilder(
        int index) {
      return getRatingFieldBuilder().getBuilder(index);
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder getRatingOrBuilder(
        int index) {
      if (ratingBuilder_ == null) {
        return rating_.get(index);  } else {
        return ratingBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public java.util.List<? extends com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder> 
         getRatingOrBuilderList() {
      if (ratingBuilder_ != null) {
        return ratingBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(rating_);
      }
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder addRatingBuilder() {
      return getRatingFieldBuilder().addBuilder(
          com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.getDefaultInstance());
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder addRatingBuilder(
        int index) {
      return getRatingFieldBuilder().addBuilder(
          index, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.getDefaultInstance());
    }
    /**
     * <pre>
     * User feedback indicating the transcription quality for one or more
     * voicemails
     * </pre>
     *
     * <code>repeated .google.internal.communications.voicemailtranscription.v1.TranscriptionRating rating = 1;</code>
     */
    public java.util.List<com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder> 
         getRatingBuilderList() {
      return getRatingFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder> 
        getRatingFieldBuilder() {
      if (ratingBuilder_ == null) {
        ratingBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRating.Builder, com.google.internal.communications.voicemailtranscription.v1.TranscriptionRatingOrBuilder>(
                rating_,
                ((bitField0_ & 0x00000001) != 0),
                getParentForChildren(),
                isClean());
        rating_ = null;
      }
      return ratingBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest)
  }

  // @@protoc_insertion_point(class_scope:google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest)
  private static final com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest();
  }

  public static com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  @java.lang.Deprecated public static final com.google.protobuf.Parser<SendTranscriptionFeedbackRequest>
      PARSER = new com.google.protobuf.AbstractParser<SendTranscriptionFeedbackRequest>() {
    @java.lang.Override
    public SendTranscriptionFeedbackRequest parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new SendTranscriptionFeedbackRequest(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<SendTranscriptionFeedbackRequest> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<SendTranscriptionFeedbackRequest> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.internal.communications.voicemailtranscription.v1.SendTranscriptionFeedbackRequest getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

